# AI Voice Filter
    A voice filter module that contains artificial intellegence

## Sound Categories

    - Air Conditioner
    - Car Horn
    - Children Playing
    - Dog Bark
    - Drilling
    - Engine Idling
    - Gun Shot
    - Jackhammer
    - Siren
    - Street Music
    - Heel, footstep sound

## Requirements
    - Tensorflow
    - Librosa
    - Numpy
    - Matplotlib